var express = require('express');
const pagesController = require('../controllers/pagesController');
const auth = require('./auth');
var router = express.Router();
const checkLogin = require("./../middlewares/checkLogin")

/* GET home page. */
router.get('/', pagesController.index);
router.get('/detail', checkLogin, pagesController.detail);

// endpoint auth
router.use("/", auth);

module.exports = router;
