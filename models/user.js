'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    /**
     * function for encrypt password
     * @param {password} password 
     * @returns 
     */
    static #encript = (password) => bcrypt.hashSync(password, 10);

    /**
     * function fo registration
     * @param {fullname, email, password} 
     * @returns 
     */
    static registration = ({ fullname, email, password }) => {
      const passwordHash = this.#encript(password);
      return this.create({
        fullname, email, password : passwordHash
      })
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({
      password, email
    }) => {
      try {
        const user = await this.findOne({ where: { email } });
        if (!user) return Promise.reject("User tidak terdaftar!");

        const isPasswordValid = user.checkPassword(password);
     
        if (!isPasswordValid) return Promise.reject("Password tidak Sesuai!");

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    }

  }
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    fullname: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};