const passport = require("passport");
const { User } = require("../models");
const LocalStrategy = require("passport-local").Strategy;

const authenticate = async (email, password, done) => {
    try {
        const user = await User.authenticate(
             { email, password  }
        );
        return done(null, user);
    } catch (error) {
        return done(null, false, { message : error })
    }
}

passport.use(
    new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate)
);

passport.serializeUser(
    (user, done) => done(null, user.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await User.findByPk(id))
);

module.exports = passport;