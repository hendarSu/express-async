
module.exports = {
    index : async (req, res, next) => {
        const data = {
            title: "Hello world!",
            subTitle: "Welcome to the world :*"
        };

        const articles = [
            {
                title : "Artikel 1",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: true
            },
            {
                title : "Artikel 2",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: true
            },
            {
                title : "Artikel 3",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: false
            }
        ];

        res.render("index", {
            page : data,
            articles : articles
        });
    },
    detail: async (req, res, next) => {
        const page = {
            title: "Artikel 1"
        }

        const articles = [
            {
                title : "Artikel 1",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: true
            },
            {
                title : "Artikel 2",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: true
            },
            {
                title : "Artikel 3",
                author : "Hendar",
                content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
                publish: true
            }
        ];

        const article = {
            title : "Artikel 1",
            author : "Hendar",
            content : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum soluta consectetur quae corrupti obcaecati dicta asperiores quibusdam, perferendis non amet unde, itaque optio voluptatum. Optio porro totam similique ipsum doloribus.",
            publish: true
        };

        res.render("detail-article", {
            article,
            page,
            articles
        });
    }
}