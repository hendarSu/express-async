const bcrypt = require('bcrypt')
const { User } = require('./../models')

const generatePassword = async (password) => {
  return await bcrypt.hash(password, 8)
}

const checkPassword = async (password, hashPassword) => {
  return await bcrypt.compare(password, hashPassword)
}

module.exports = {
  index: async (req, res, next) => {
    // Function rendering for page registrasi user.
    res.render("registration", { page : {
        title : "Halaman Registrasi"
    } })
  },
  registration1: async (req, res, next) => {
    const { email, password, fullname } = req.body
    const passwordHash = await generatePassword(password)
    const user = new Promise((reslove, reject) => {
      User.create({
        email,
        password: passwordHash,
        fullname,
      })
        .then((data) => {
          reslove(data)
        })
        .catch((err) => {
          reject(err)
        })
    })

    user
      .then((data) => {
        delete data.password;
        res.status(201).json({
          status: 'success',
          code: 1,
          message: 'Registration success!',
          data: data,
        })
      })
      .catch((err) => {
        res.status(400).json({
          status: 'fail',
          code: 0,
          message: 'Registration unsuccess!',
          data: err.message,
        })
      })
  },
  registration2: async (req, res, next) => {
    const { email, password, fullname } = req.body
    try {

      const user = await User.registration({
        fullname,
        email,
        password
      })

      delete user.password;
      res.status(201).json({
        status: 'success',
        code: 1,
        message: 'Registration success!',
        data: user,
      })
    } catch (error) {
      res.status(400).json({
        status: 'fail',
        code: 0,
        message: 'Registration unsuccess!',
        data: error.errors
      })
    }
  }
}
